﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using AttentiChallenge.Models;
using AttentiChallenge.Services;
using Microsoft.AspNetCore.Mvc;

namespace AttentiChallenge.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BitmapController: ControllerBase
    {
        private readonly IBitmapService _service;
        private readonly IBitmapCache _cache;

        public BitmapController(IBitmapService bitmapService, IBitmapCache cache)
        {
            _service = bitmapService;
            _cache = cache;
        }

        // POST api/bitmap
        [HttpPost]
        public ActionResult<Bitmap> Post([FromBody] CreateBitmapRequest value)
        {
            return _service.Create(value);
        }

        // GET api/bitmap/5
        [HttpGet("{id}")]
        public ActionResult<Bitmap> Get(Guid id)
        {
            return _cache.Get(id);
        }


        [Route("{id}/resolved.png")]
        [HttpGet]
        public IActionResult ResolveBitmap(Guid id)
        {
            var retVal = _cache.GetResolvedBitmap(id);
            

            using (MemoryStream ms = new MemoryStream())
            {
                retVal.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);

                return File(ms.ToArray(), "image/png");
                //HttpResponseMessage result = new HttpResponseMessage(System.Net.HttpStatusCode.OK);
                //result.Content = new ByteArrayContent(ms.ToArray());
                //result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");
                //return result; 
            }
        }

        [Route("{id}/resolve")]
        [HttpPost]
        public IActionResult ResolveBitmap(Guid id, [FromBody] CreateBitmapRequest value)
        {
            var retVal = _service.ResolveToImage(id, value.Width, value.Height);

            return Ok(retVal);
            //using (MemoryStream ms = new MemoryStream())
            //{
                //retVal.Save(ms, System.Drawing.Imaging.ImageFormat.Png);

                //return File(ms.ToArray(), "image/png");
                //HttpResponseMessage result = new HttpResponseMessage(System.Net.HttpStatusCode.OK);
                //result.Content = new ByteArrayContent(ms.ToArray());
                //result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");
                //return result; 
            //}
        }
    }
}
