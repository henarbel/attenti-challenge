﻿function Bitmap(id, width, height, pixelsON) {
    this.id = id;
    this.width = width;
    this.height = height;
    this.pixelsON = pixelsON;
    // Box width
    this.bw = 500;
    // Box height
    this.bh = 500;
    // Padding
    this.padding = 10;
    this.lineWidth = 5 / width;
    this.context = null;
    this.pixelWidth = this.bw / this.width;
    this.pixelHeight = this.bh / this.height;
    this._colorCache = {};
}

Bitmap.prototype.drawOnCanvas = function(canvas, skeletonOnly) {

    var p = this.padding;
    var context = canvas.getContext("2d");
    context.lineWidth = this.lineWidth;

    for (var x = 0; x <= this.bw; x += this.pixelWidth) {
        context.moveTo(this.lineWidth + x + p, p);
        context.lineTo(this.lineWidth + x + p, this.bh + p);
    }


    for (var x = 0; x <= this.bh; x += this.pixelHeight) {
        context.moveTo(p, this.lineWidth + x + p);
        context.lineTo(this.bw + p, this.lineWidth + x + p);
    }

    context.strokeStyle = "black";
    context.stroke();
    this.context = context;
    var self = this;
    if(skeletonOnly) {
        return;
    }
    this.pixelsON.forEach(function(pixel) {
        self.paintPixel('#000', pixel.x, pixel.y);
    });
};

Bitmap.prototype.paintPixel = function(color, x, y) {
    this.context.fillStyle = color;
    this.context.fillRect(
        this.padding + this.lineWidth + (x * this.pixelWidth),
        this.padding + this.lineWidth + (y * this.pixelHeight), 
        this.pixelWidth, 
        this.pixelHeight);
};

Bitmap.prototype.intColorToHex = function(i) {
    var resolvedColor = this._colorCache[i];
    if(resolvedColor === undefined) {
        var hex = (i & 0xFF).toString(16) +
                  ((i >> 8) & 0xFF).toString(16) +
                ((i >> 16) & 0xFF).toString(16) +
                ((i >> 24) & 0xFF).toString(16);

        hex += '000000'; // pad result
        hex = hex.substring(0, 6);
        resolvedColor = "#" + hex;
        this._colorCache[i] = resolvedColor;
    }
    return resolvedColor;
};

Bitmap.prototype.paintIsland = function(root) {
    var self = this;
    function _recursivePaint(pixel) {
        self.paintPixel(self.intColorToHex(pixel.color), pixel.x, pixel.y);
        pixel.neighbors.forEach(function(x) { self.paintIsland(x) });
    }
    _recursivePaint(root);
};

Bitmap.prototype.bindCanvasEvents = function() {
    this.pixelsON = [];
    var mouseIsDown = false;
    var self = this;
    var left = self.padding - self.lineWidth;
    var right = self.bw - self.padding - self.lineWidth;
    var top = left;
    var bottom = self.bh - self.padding - self.lineWidth;
    var pixelsAdded = {};
    function _tryAddPixel(x, y) {
        console.log("x-y", x, y);
        if(x > left && x < right && y > top && y < bottom){
            var pixelX = Math.round(x / self.pixelWidth) - 1;
            var pixelY = Math.round(y / self.pixelHeight) - 1;
            console.log("got pixel", pixelX, pixelY);
            if(pixelsAdded[x] === undefined || pixelsAdded[x][y] === undefined)  {
                pixelsAdded[pixelX] = pixelsAdded[pixelX] || {};
                pixelsAdded[pixelX][pixelY] = 1;
                console.log("adding pixel", pixelX, pixelY);
                var pxl = {x: pixelX, y: pixelY }
                self.pixelsON.push(pxl);
                self.paintPixel('#000', pixelX, pixelY);
            }
        }
    }
    function _getXY(e) {
        var x;
        var y;
        if (e.pageX || e.pageY) { 
          x = e.pageX;
          y = e.pageY;
        }
        else { 
          x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft; 
          y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop; 
        } 
        x -= self.context.canvas.offsetLeft;
        y -= self.context.canvas.offsetTop;
        return [x,y];
    }
    this.context.canvas.onmousedown = function(e){
        mouseIsDown = true;
        var xy = _getXY(e);
        _tryAddPixel(xy[0], xy[1]);
    }
    this.context.canvas.onmouseup = function(e){
        mouseIsDown = false;
    }
    this.context.canvas.onmousemove = function(e){
        if(!mouseIsDown) return;

        var xy = _getXY(e);
        _tryAddPixel(xy[0], xy[1]);
        return false;
    }
};

(function() {

    var container=document.getElementById('container');
    var currentBitmap = null;

    function _createNewBitmap(size, pixels, rand, cb) {
        axios.post('/api/bitmap', {
            width: size[0],
            height: size[1],
            randomize: rand,
            pixels: pixels
          })
        .then(function(response) {
            currentBitmap = new Bitmap(response.data.id, size[0], size[1], response.data.pixelsON);
            cb();
        });
    }

    function showPage1() {
        container.innerHTML = ["<h2>Hello</h2>",
        "<p>Please Enter Bitmap size:</p>",
        "<div class=\"form-item\">",
        "    <input id=\"size\" type=\"text\" placeholder=\"Bitmap size: n,m\" />",
        "</div>",
        "<div class=\"form-item\">",
        "    <button id=\"randomize\">Randomize</button>",
        "</div>",
        "<div class=\"form-item\">",
        "    <button id=\"bonusdraw\">BONUS DRAW</button>",
        "</div>"].join('\n');
        function _getSize() {
            var textbox = document.getElementById("size");
            var size = textbox.value.replace(' ', '').split(',')
                .map(x => parseInt(x));

            if(!size[0] || !size[1]) {
                alert("invalid input");
                return false;
            }
            return size;
        }
        document.getElementById("randomize").addEventListener("click", function() {
            var size = _getSize();
            if(!size) {
                return;
            }
            _createNewBitmap(size, [], true, showPage2);
        });
        document.getElementById("bonusdraw").addEventListener("click", function() {
            var size = _getSize();
            if(!size) {
                return;
            }
            currentBitmap = new Bitmap(null, size[0], size[1], []);
            showPage4();
        });

    }

    function showPage2() {
        container.innerHTML = [
        "<canvas id=\"canvas\" width=\"520px\" height=\"520px\"></canvas>",
        "<div class=\"form-item\">",
        "    <button id=\"solve\">Solve</button>",
        "</div>"].join('\n');
        var canvas = document.getElementById("canvas");
        currentBitmap.drawOnCanvas(canvas);
        document.getElementById("solve").addEventListener('click', function() {
            axios.post('/api/bitmap/' + currentBitmap.id + '/resolve/', { Width: 500, Height: 500 })
                .then(function(response) {
                    showPage3(response.data);
            });
        });
    }

    function showPage3(islands) {
        container.innerHTML = [
        "<h3>Found " + islands.islandCount + " Islands</h3>",
        "<div class='resolved'>",
        "<img src='" + '/api/bitmap/' + currentBitmap.id + '/resolved.png' + "' width=\"500px\" height=\"500px\"></img>",
        "</div>",
        "<div class=\"form-item\">",
        "    <button id=\"restart\"><- Restart</button>",
        "</div>"].join('\n');
        document.getElementById("restart").addEventListener('click', showPage1);
    }

    function showPage4() {
        container.innerHTML = [
        "<canvas id=\"canvas\" width=\"520px\" height=\"520px\"></canvas>",
        "<div class=\"form-item\">",
        "    <button id=\"solve\">Solve</button>",
        "</div>"].join('\n');
        var canvas = document.getElementById("canvas");
        currentBitmap.drawOnCanvas(canvas, true);
        currentBitmap.bindCanvasEvents();
        document.getElementById("solve").addEventListener('click', function() {
            _createNewBitmap([currentBitmap.width, currentBitmap.height], currentBitmap.pixelsON, false, function(){
                axios.post('/api/bitmap/' + currentBitmap.id + '/resolve/', { Width: 500, Height: 500 })
                    .then(function(response) {
                        showPage3(response.data);
                 });
            })
        });
    }

    showPage1();
})();