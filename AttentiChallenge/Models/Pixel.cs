﻿using System;
using System.Collections.Generic;

namespace AttentiChallenge.Models
{
    public class SimplePixel
    {
        public SimplePixel(int x, int y)
        {
            X = x;
            Y = y;
        }
        public int X { get; set; }
        public int Y { get; set; }
    }



    public class Pixel: SimplePixel
    {
        public Pixel(int x, int y, int color): base(x, y)
        {
            IsBlack = color > 0;
            Color = color;
        }

        public IEnumerable<Pixel> Neighbors { get; set; }
        public bool IsBlack { get; }
        public int Color { get; }
    }
}
