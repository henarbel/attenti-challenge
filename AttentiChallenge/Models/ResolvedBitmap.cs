﻿using System;
using System.Drawing;
using System.Runtime.Serialization;

namespace AttentiChallenge.Models
{
    [DataContract]
    public class ResolvedBitmap
    {
        public ResolvedBitmap(Image image, int islandCount)
        {
            IslandCount = islandCount;
            Image = image;

        }

        [DataMember]
        public int IslandCount { get; }

        public Image Image { get; }
    }
}
