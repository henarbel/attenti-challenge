﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace AttentiChallenge.Models
{
    [DataContract]
    public class Bitmap
    {
        public Bitmap(Guid UUID, bool[,] matrix,
                      IEnumerable<SimplePixel> pixelsON)
        {
            ID = UUID;
            Matrix = matrix;
            PixelsON = pixelsON;
        }

        [DataMember]
        public Guid ID { get; }

        public bool[,] Matrix { get; }

        [DataMember]
        public IEnumerable<SimplePixel> PixelsON { get; }

        [DataMember]
        public int Width
        {
            get
            {
                return Matrix.GetLength(0);
            }
        }

        [DataMember]
        public int Height
        {
            get
            {
                return Matrix.GetLength(1);
            }
        }
    }
}
