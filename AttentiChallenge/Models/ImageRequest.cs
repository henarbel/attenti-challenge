﻿using System;
namespace AttentiChallenge.Models
{
    public class ImageRequest
    {
        public int Width { get; set; }

        public int Height { get; set; }
    }
}
