﻿using System;
using System.Collections.Generic;

namespace AttentiChallenge.Models
{
    public class BitmapInfo
    {
        public BitmapInfo(
            Guid id,
            int width,
            int height,
            IEnumerable<SimplePixel> pixelsON)
        {
            Width = width;
            Height = height;
            PixelsON = pixelsON;

        }

        public int Width { get; }
        public int Height { get; }
        public IEnumerable<SimplePixel> PixelsON { get; }
    }
}
