﻿using System;
using System.Collections.Generic;

namespace AttentiChallenge.Models
{
    public class CreateBitmapRequest
    {
        public CreateBitmapRequest(
            int width,
            int height,
            IEnumerable<SimplePixel> pixels,
            bool randomize)
        {
            Width = width;
            Height = height;
            Randomize = randomize;
            Pixels = pixels;
        }

        public int Width { get; }

        public int Height { get; }

        public bool Randomize { get; }

        public IEnumerable<SimplePixel> Pixels { get; }
    }

}
