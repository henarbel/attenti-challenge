﻿using System;
using System.Collections.Generic;
using AttentiChallenge.Models;

namespace AttentiChallenge.Services
{
    public interface IBitmapCache
    {
        Bitmap Get(Guid uuid);

        void Set(Bitmap bitmap);

        void PutResolvedBitmap(Guid uuid, ResolvedBitmap resolvedBitmap);

        ResolvedBitmap GetResolvedBitmap(Guid uuid);
    }

    public class BitmapCache: IBitmapCache
    {
        private readonly Bitmap[] _cache;
        private readonly ResolvedBitmap[] _resolvedBitmaps;
        private readonly IDictionary<Guid, int> _keys;
        private int _nextIndex;

        public BitmapCache(int size)
        {
            _cache = new Bitmap[size];
            _resolvedBitmaps = new ResolvedBitmap[size];
            _keys = new Dictionary<Guid, int>();
            _nextIndex = 0;
        }

        public Bitmap Get(Guid uuid)
        {
            int index;
            if (_keys.TryGetValue(uuid, out index))
            {
                return _cache[index];
            }
            return null;
        }

        public ResolvedBitmap GetResolvedBitmap(Guid uuid)
        {
            int index;
            if (_keys.TryGetValue(uuid, out index))
            {
                return _resolvedBitmaps[index];
            }
            return null;
        }

        public void PutResolvedBitmap(Guid uuid, ResolvedBitmap resolvedBitmap)
        {
            int index;
            if (_keys.TryGetValue(uuid, out index))
            {
                _resolvedBitmaps[index] = resolvedBitmap;
            }
        }

        public void Set(Bitmap bitmap)
        {
            lock (_cache)
            {
                if (_cache[_nextIndex] != null)
                {
                    _keys.Remove(_cache[_nextIndex].ID);
                }
                _cache[_nextIndex] = bitmap;
                _keys[bitmap.ID] = _nextIndex;
                _nextIndex = ++_nextIndex % _cache.Length;
            }
        }
    }
}
