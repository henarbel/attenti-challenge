﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;

namespace AttentiChallenge.Models
{
    class IslandBitmapResolver : IEnumerable<Pixel>
    {
        private readonly Bitmap _bitmap;

        public IslandBitmapResolver(Bitmap bitmap)
        {
            _bitmap = bitmap;
        }
        public IEnumerator<Pixel> GetEnumerator()
        {
            var pxls = new BitmapIslandPixels(_bitmap);
            while(pxls.MoveNext())
            {
                //iterate all
            }
            return pxls.Islands.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }


    class BitmapIslandPixels : IEnumerator<Pixel>
    {
        private Pixel _currentIslandPixel;
        private IslandEnumerable _currentIsland;
        private Bitmap _bitmap;
        private readonly bool[,] _visitedItems;
        private HashSet<System.Drawing.Color> _colors;
        private readonly Random _rnd;
        private LinkedList<Pixel> _islands;
        private Color _currentColor;

        public BitmapIslandPixels(Bitmap bitmap)
        {
            _bitmap = bitmap;
            _visitedItems = new bool[
                bitmap.Width,
                bitmap.Height
            ];
            _colors = new HashSet<System.Drawing.Color>();
            _rnd = new Random();
            _currentColor = System.Drawing.Color.White;
            _islands = new LinkedList<Pixel>();
        }

        private System.Drawing.Color genColor()
        {
            System.Drawing.Color color = System.Drawing.Color.White;

            while (color == System.Drawing.Color.White || _colors.Contains(color))
            {
                color = System.Drawing.Color.FromArgb(
                                _rnd.Next(256),
                                _rnd.Next(256),
                                _rnd.Next(256));

            }
            _currentColor = color;
            _colors.Add(color);
            return color;
        }

        public System.Drawing.Color Color => _currentColor;

        public IEnumerable<Pixel> Islands => _islands;

        private bool applyCurrentIsland()
        {
            var fromX = _currentIslandPixel == null ? 0 : _currentIslandPixel.X;
            var fromY = _currentIslandPixel == null ? 0 : _currentIslandPixel.Y + 1;
            int k = fromY;
            for (int j = fromX; j < _bitmap.Width; j++)
            {
                for (; k < _bitmap.Height; k++)
                {
                    if (!_visitedItems[j, k] && _bitmap.Matrix[j, k])
                    {
                        var randomColor = genColor();
                        var pixel = new Pixel(j, k, randomColor.ToArgb());
                        _visitedItems[j, k] = true;
                        _islands.AddFirst(pixel);
                        _currentIslandPixel = pixel;
                        _currentIsland = new IslandEnumerable(
                            _currentIslandPixel,
                            _bitmap,
                            _visitedItems);
                        return true;
                    }
                }
                k = 0;
            }
            _currentIslandPixel = null;
            _currentIsland = null;
            return false;
        }

        public Pixel Current => _currentIsland.Current;

        object IEnumerator.Current => _currentIsland.Current;

        public void Dispose()
        {
        }

        public bool MoveNext()
        {
            if (_currentIsland != null)
            {
                var retVal = _currentIsland.MoveNext();
                return retVal || applyCurrentIsland();
            }
            return applyCurrentIsland();
        }

        public void Reset()
        {
            _currentIslandPixel = null;
        }
    }

    public class IslandEnumerable : IEnumerator<Pixel>
    {
        private static readonly int[] _rowNeigbor = new int[] {-1, -1, -1, 0,
                                   0, 1, 1, 1};
        private static readonly int[] _colNeighbor = new int[] {-1, 0, 1, -1,
                                   1, -1, 0, 1};

        private Pixel _root;
        private LinkedList<Pixel> _allPixels;
        private bool[,] _visitedItems;
        private Bitmap _bitmap;
        private Pixel _current;
        private Pixel _currentNegihbor;
        private LinkedList<Pixel> _currentPixels;
        private int _naighborsIdx;

        public IslandEnumerable(Pixel root, Bitmap bitmap, bool[,] visitedItems)
        {
            _root = root;
            _allPixels = new LinkedList<Pixel>();
            _visitedItems = visitedItems;
            _bitmap = bitmap;
            _allPixels.AddFirst(root);
            _currentNegihbor = root;
        }

        public Pixel Current => _currentNegihbor;

        object IEnumerator.Current => _currentNegihbor;

        public void Dispose()
        {
            
        }

        private bool canVisitNeighbor(int x, int y)
        {
            return ((x >= 0) && (x < _bitmap.Width) &&
                (y >= 0) && (y < _bitmap.Height) &&
                (_bitmap.Matrix[x, y] && !_visitedItems[x, y]));
        }

        private bool applyNextNeighbor()
        {
            if(_current == null)
            {
                return false;
            }
            for (; _naighborsIdx < _rowNeigbor.Length; _naighborsIdx++)
            {
                var x = _current.X + _rowNeigbor[_naighborsIdx];
                var y = _current.Y + _colNeighbor[_naighborsIdx];
                if (canVisitNeighbor(x, y))
                {
                    _visitedItems[x, y] = true;
                    var pxl = new Pixel(x, y, _current.Color);
                    _currentPixels.AddLast(pxl);
                    _allPixels.AddLast(pxl);
                    _currentNegihbor = pxl;
                    return true;
                }
            }
            _current.Neighbors = _currentPixels;
            return false;
        }

        public bool applyCurrent()
        {
            _current = null;
            _currentNegihbor = null;

            while (_allPixels.Count > 0)
            {
                _current = _allPixels.First.Value;
                _allPixels.RemoveFirst();
                _currentPixels = new LinkedList<Pixel>();
                _naighborsIdx = 0;
                return true;
            }
            return false;
        }

        public bool MoveNext()
        {
            var retVal = applyNextNeighbor();
            if (!retVal)
            {
                while (applyCurrent())
                {
                    if (applyNextNeighbor())
                    {
                        return true;
                    }
                }
                return false;
            }
            return retVal;
        }

        public void Reset()
        {
            throw new NotImplementedException();
        }

    }

}
