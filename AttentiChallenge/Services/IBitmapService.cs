﻿using System;
using AttentiChallenge.Models;
using System.Collections.Generic;
using System.Linq;

namespace AttentiChallenge.Services
{
    public interface IBitmapService
    {
        Bitmap Create(CreateBitmapRequest request);

        IEnumerable<Pixel> Resolve(Guid id);

        ResolvedBitmap ResolveToImage(Guid id, int width, int height);
    }

    public class BitmapServiceImp: IBitmapService
    {
        private readonly IBitmapCache _cache;
        private readonly int[] _rowNeigbor = new int[] {-1, -1, -1, 0,
                                   0, 1, 1, 1};
        private readonly int[] _colNeighbor = new int[] {-1, 0, 1, -1,
                                   1, -1, 0, 1};

        public BitmapServiceImp(IBitmapCache cache)
        {
            _cache = cache;
        }

        private int _genRandomIsland(int maxPixel, SimplePixel center,
                                      List<SimplePixel> pixelsON,
                                      Random rand, Bitmap bitmap)
        {
            int currentPixelCount = 0;
            List<SimplePixel> neigborsToVisit = new List<SimplePixel>();
            bool[,] centered = new bool[bitmap.Width, bitmap.Height];
            centered[center.X, center.Y] = true;
            while (currentPixelCount < maxPixel)
            {
                var minX = Math.Max(0, center.X - 1);
                var maxX = Math.Min(center.X + 2, bitmap.Width);
                var minY = Math.Max(0, center.Y - 1);
                var maxY = Math.Min(center.Y + 2, bitmap.Height);

                List<SimplePixel> addedPixels = new List<SimplePixel>();
                for (int x = minX; x < maxX; x++)
                {
                    for (int y = minY; y < maxY; y++)
                    {

                        if (!bitmap.Matrix[x,y])
                        {
                            bool isCenter = x == center.X && y == center.Y;
                            SimplePixel pxl = isCenter ? center : new SimplePixel(x, y);

                            if (!isCenter)
                                addedPixels.Add(pxl);
                            pixelsON.Add(pxl);
                            bitmap.Matrix[pxl.X, pxl.Y] = true;
                            currentPixelCount++;
                        }
                    }
                }

                if (addedPixels.Count == 0)
                {
                    bool foundNeighbor = false;
                    while (neigborsToVisit.Count > 0)
                    {
                        center = neigborsToVisit[0];
                        neigborsToVisit.RemoveAt(0);
                        if (!centered[center.X, center.Y])
                        {
                            foundNeighbor = true;
                            break;
                        }
                    }
                    if (!foundNeighbor)
                        break;
                }
                else
                {
                    int nextIndx = rand.Next(0, addedPixels.Count);
                    center = addedPixels[nextIndx];
                    centered[center.X, center.Y] = true;
                    addedPixels.RemoveAt(nextIndx);
                    neigborsToVisit.AddRange(addedPixels);
                }
            }
            return currentPixelCount;
        }

        public Bitmap Create(CreateBitmapRequest request)
        {
            bool[,] matrix = new bool[request.Width, request.Height];
            List<SimplePixel> pixelsON = new List<SimplePixel>();
            var retVal = new Bitmap(Guid.NewGuid(), matrix, pixelsON);

            if (request.Randomize)
            {
                Random rand = new Random();
                int islandCount = rand.Next(1, Math.Min(request.Width, request.Height) / 2);
                int randIslandMaxSize = Math.Max(request.Width, (int)Math.Floor(0.001 * request.Width * request.Height));

                for (int i = 0; i < islandCount; i++)
                {
                    int maxPixels = rand.Next(1, randIslandMaxSize);
                    var x = rand.Next(request.Width);
                    var y = rand.Next(request.Height);
                    var addedPixles = _genRandomIsland(maxPixels, new SimplePixel(x, y),
                        pixelsON,
                        rand,
                        retVal);
                }
            }

            //didnt put else here so the user can choose randomize and also specific pixels
            foreach (var pixel in request.Pixels)
            {
                if (!matrix[pixel.X, pixel.Y])
                {
                    matrix[pixel.X, pixel.Y] = true;
                    pixelsON.Add(pixel);
                }
            }
            _cache.Set(retVal);
            return retVal;
        }

        public IEnumerable<Pixel> Resolve(Guid id)
        {
            List<Pixel> pixels = new List<Pixel>();

            Bitmap bitmap = _cache.Get(id);
            return new IslandBitmapResolver(bitmap);
        }

        public ResolvedBitmap ResolveToImage(Guid id, int width, int height)
        {
            Bitmap bitmap = _cache.Get(id);
            var rectWidth = width / (float)bitmap.Width;
            var rectHeight = height/ (float)bitmap.Height;

            System.Drawing.Image img = new System.Drawing.Bitmap(width, height);
            System.Drawing.Graphics drawing = System.Drawing.Graphics.FromImage(img);

            var resolver = new BitmapIslandPixels(bitmap);
            System.Drawing.SolidBrush brush = null;

            while (resolver.MoveNext())
            {
                if (brush == null || resolver.Color != brush.Color)
                {
                    brush = new System.Drawing.SolidBrush(resolver.Color);
                }
                drawing.FillRectangle(brush, resolver.Current.X * rectWidth,
                    resolver.Current.Y * rectHeight,
                    rectWidth,
                    rectHeight);

            }
            var retVal = new ResolvedBitmap(img, resolver.Islands.Count());
            _cache.PutResolvedBitmap(id, retVal);
            return retVal;

        }
    }
}
